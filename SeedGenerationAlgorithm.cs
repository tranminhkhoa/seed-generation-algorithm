using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System.Numerics;
using System.Collections;
using System.Globalization;
using System.IO;

namespace SeedGeneration
{

    class Utilities
    {
        public static string MD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("x2"));
                }
                return sb.ToString();
            }
        }

        public static string SHA256(string inputString)
        {
            SHA256 sha256 = SHA256Managed.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(inputString);
            byte[] hash = sha256.ComputeHash(bytes);
            return GetStringFromHash(hash);
        }

        public static string SHA512(string inputString)
        {
            SHA512 sha512 = SHA512Managed.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(inputString);
            byte[] hash = sha512.ComputeHash(bytes);
            return GetStringFromHash(hash);
        }

        private static string GetStringFromHash(byte[] hash)
        {
            StringBuilder result = new StringBuilder();
            foreach (byte b in hash)
            {
                result.Append(b.ToString("x2"));
            }
            return result.ToString();
        }

        public static int GetHexVal(char hex)
        {
            int val = (int)hex;
            //For uppercase A-F letters:
            return val - (val < 58 ? 48 : 55);
            //For lowercase a-f letters:
            //return val - (val < 58 ? 48 : 87);
            //Or the two combined, but a bit slower:
            //return val - (val < 58 ? 48 : (val < 97 ? 55 : 87));
        }

        public static byte[] HexStringToBytes(string hex)
        {
            if (hex.Length % 2 == 1)
                throw new Exception("The binary key cannot have an odd number of digits");

            byte[] arr = new byte[hex.Length >> 1];

            for (int i = 0; i < hex.Length >> 1; ++i)
            {
                arr[i] = (byte)((GetHexVal(hex[i << 1]) << 4) + (GetHexVal(hex[(i << 1) + 1])));
            }

            return arr;
        }

        public static BitArray ConvertHexToBitArray(string hexData)
        {
            if (hexData == null)
                return null; // or do something else, throw, ...

            BitArray ba = new BitArray(4 * hexData.Length);
            for (int i = 0; i < hexData.Length; i++)
            {
                byte b = byte.Parse(hexData[i].ToString(), NumberStyles.HexNumber);
                for (int j = 0; j < 4; j++)
                {
                    ba.Set(i * 4 + j, (b & (1 << (3 - j))) != 0);
                }
            }
            return ba;
        }

        public static void printBits(BitArray bits)
        {
            foreach(bool b in bits)
                if (b)
                    Console.Write(1);
                else
                    Console.Write(0);
            /*for (int i = 1; i <= bits.Length; i++)
            {
                if (bits[i - 1])
                    Console.Write(1);
                else
                    Console.Write(0);
                if (i % 11 == 0)
                    Console.Write(" ");
            }
            Console.WriteLine();*/
        }
    }

    class AskAnswerProtocol
    {
        private string QuestionSetFileName;
        private int[] QuestionIndices;
        private string[] AnswerList;

        public AskAnswerProtocol(string QuestionSetFileName)
        {
            this.QuestionSetFileName = QuestionSetFileName;
            QuestionIndices = new int[12];
            AnswerList = new string[12];
        }
        public void AskQuestions(int[] QuestionIndices)
        {
            Console.WriteLine("\n3. LET Q BE A SEQUENCE OF QUESTIONS. FOR EACH IDX IN Q-INDICES, ASK QUESTION Q[IDX] TO USER");
            for (int i = 0; i < 12; i++)
            {
                string question = File.ReadLines(QuestionSetFileName).Skip(QuestionIndices[i] - 1).Take(1).First();
                Console.WriteLine("\nQuestion " + (i + 1) + ". " + question);
                AnswerList[i] = Console.ReadLine();
            }
        }

        public string GetAnswers()
        {
            string answers = AnswerList[0];
            for (int i = 1; i < 12; i++)
            {
                answers = answers + "|" + AnswerList[i];
            }
            return answers;
        }
    }
    class SeedGenerationAlgorithm
    {
        private string sk;

        SeedGenerationAlgorithm(string msk)
        {
            sk = Utilities.MD5(msk);
        }
 
        public int[] GetQuestionIndices()
        {
            int[] questionIndices = new int[12];
            Array.Clear(questionIndices, 0, questionIndices.Length);
            BitArray skBits = Utilities.ConvertHexToBitArray(sk);

            Console.WriteLine("1. STRETCH THE MASTER SECRET KEY (MSK) TO 128-BIT SECRET KEY (SK) BY MD5 HASHING ALGORITHM\n");            
            Console.Write("\tSK = ");
            Utilities.printBits(skBits);
            Console.Write("|");
            Console.WriteLine(sk);

            BitArray chksumBits = Utilities.ConvertHexToBitArray(Utilities.SHA256(sk));
            BitArray msk_plus_chksum = new BitArray(132);
            BitArray question_idx = new BitArray(11);
            int i = 0;
            while(i < 128)
            {
                msk_plus_chksum.Set(i, skBits.Get(i));
                i++;
            }
            for (int j = 0; j < 4; j++)
            {
                msk_plus_chksum.Set(i, chksumBits.Get(j));
                i++;
            }

            Console.WriteLine("\n2. CONCATENATE THE FIRST FOUR BITS OF SHA256(SK) (FOR CHECKSUM) TO THE END OF SK\n");
            Console.Write("\t- SHA256(SK) \t= ");
            Utilities.printBits(chksumBits);
            Console.Write("|");
            Console.WriteLine(Utilities.SHA256(sk));
            Console.Write("\t- CS \t\t= ");
            for (i = 0; i < 4; i++)
            {
                if (chksumBits.Get(i))
                    Console.Write(1);
                else
                    Console.Write(0);
            }
            Console.Write("\n\t- SK = SK||CS \t= ");
            Utilities.printBits(msk_plus_chksum);

            int m = 1, k = -1;
            for (i = msk_plus_chksum.Length - 1; i >= 0; i--)
            {
                if ((i + 1) % 11 == 0)
                {
                    m = 1;
                    k++;
                }
                if (msk_plus_chksum.Get(i))
                    questionIndices[k] += m;
                m <<= 1;
            }
            Console.WriteLine("\n\n3. BREAK SK INTO 12 BLOCKS, EACH BLOCK CONSISTS OF 11 BITS ENCODING AN INDEX OF A QUESTION IN THE QUESTION SET");
            Console.Write("\n\t- Q-INDICES \t= { " + questionIndices[0]);
            for (i = 1; i < 12; i++)
            {
                Console.Write(", " + questionIndices[i]);
            }
            Console.WriteLine(" }");
            return questionIndices;
        }

        /*public static string GetSeed(string[] answerList, string sk)
        {
            string answers = answerList[0];
            for (int i = 1; i < answerList.Length; i++)
            {
                answers = answers + "|" + answerList[i];
            }
            Console.WriteLine(answers);
            return GetSeed(answers, sk);
        }
        public static string GetSeed(string answers, string msk)
        {
            var pbkdf2 = new Rfc2898DeriveBytes(Encoding.UTF8.GetBytes(answers), HexStringToBytes(MD5(msk)), 2048);
            return Convert.ToBase64String(pbkdf2.GetBytes(32));
        }*/

        public string GetSeed(string answers)
        {
            Console.Write("\n5. GET YOUR SEED\n\tSEED = ");
            return Utilities.SHA512(answers);
            //return Convert.ToBase64String(KeyDerivation.Pbkdf2(answers, Utilities.HexStringToBytes(sk), KeyDerivationPrf.HMACSHA512, 2048, 512 / 8));
        }
        public string GetSeed(string[] answerList)
        {
            string answers = answerList[0];
            for (int i = 1; i < answerList.Length; i++)
            {
                answers = answers + "|" + answerList[i];
            }
            Console.WriteLine(answers);
            return GetSeed(answers);
        }
        static void Main(string[] args)
        {
            string msk = "1234";
            SeedGenerationAlgorithm sga = new SeedGenerationAlgorithm(msk);
            AskAnswerProtocol aap = new AskAnswerProtocol("English.txt");
            aap.AskQuestions(sga.GetQuestionIndices());
            Console.WriteLine(sga.GetSeed(aap.GetAnswers()));
        }
    }
}
